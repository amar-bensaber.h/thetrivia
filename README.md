![Trivia](http://image.noelshack.com/fichiers/2019/02/5/1547199472-capture-d-ecran-2019-01-11-a-10-37-36.png)

- Project : Using a specific API to set up a quiz with React

## What is it ? 

It's a quizz on which you have to acquire a score of 10 to win the game but beware if you're wrong 3 times , YOU LOOSE !

## Rules

You have the choice between 10 categories, when you choose 1 category you have a question and a field to answer it, you're score increment by 1 if you're right but if you're wrong you're attempts increment by 1, if you're attempts are equal to 3 you loose and if you're score is equal to 10 you win.

You can navigate in the 10 categories to acquire you're score of 10, it's not a quizz where you have to become a score in only 1 category, it's global !

You're score will be save, you can leave the game and come back later to continue ! 

Don't worry, if you win or loose, you will be able to play again 😋

## Installation and use 

- Clone the repository 
- In terminal, cd into the directory you cloned into
- Use the package manager [npm](https://www.npmjs.com/) to install Trivia Quiz.
```bash
npm install
```
- To run it : 
```bash
npm start
```
- Visit http://localhost:3000/ and enjoy !

## Features
- Retrieve questions for jService API and display them
- Possibility to choose a category among those available on the API / save the choice in localStorage and could change category inside a category
- View a question, can answer it
- Never show the same questions, if a question is right then next to the other one
- Display a score, save it in localStorage, if this score is equal to 10 you win, if 3 attempts score is reset to 0, possibility to reset the score inside a category question
- Display an attempts number, save it in localStorage, if 3 attempts, you loose
- If the user failed to an answer, he have a possibility to check the answer to continue
- Reload the page give us the last state of the quizz, our score, attemps and current questions

## Project Team

> Hichem AMAR BENSABER

> Thomas FRANJA

> Alice FABRE

> Mickael DE JESUS
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


