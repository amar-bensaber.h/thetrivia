import React from 'react';
import {Link} from 'react-router-dom';
import '../../styles/Welcoming.scss';
import logoItem from '../../assets/img/logoTrivia.svg';

const Welcoming = () => {

    return (
        <div className="container">
            <div className="container-logo">
                <img src={logoItem} alt="Logo Trivia" className="container-logo__item"/>
            </div>
            <div className="container-title">
                <h1 className="container-title__description">
                    Mesurez vous aux différentes catégories du Trivia Quizz
                </h1>
            </div>
            <Link className="container-btn__description" to={'/home'}>
                <button className="container-btn">
                    Commencer
                </button>
            </Link>
        </div>

    );
};

export default Welcoming;