import React from 'react';
import {Link} from 'react-router-dom';
import '../../styles/Win.scss';


const Win = () => {

    const resetState = () => {
        localStorage.setItem('score', 0);
        localStorage.setItem('miss', 0);
    };

    return (
        <div className="win-container">
            <div className="win-container-background"></div>
            <div className="win-container__text">
                <h1 className="win-container__text__title">Win !</h1>
                <p className="win-container__text__description">Bravo, vous avez réussi le Trivia Quizz avec un score
                    de 10/10</p>
            </div>
            <div className="win-container__redirect">
                <Link className="win-container__redirect__button__link" to={'/home'}
                      onClick={() => resetState()}>
                    <button className="win-container__redirect__button" type="button">
                        Recommencer
                    </button>
                </Link>
            </div>
        </div>

    );
};

export default Win;