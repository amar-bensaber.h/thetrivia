import React from 'react';
import {Link} from 'react-router-dom';
import '../../styles/Loose.scss';
import gameOver from '../../assets/img/GameOver.svg';


const Loose = () => {

    const resetState = () => {
        localStorage.setItem('score', 0);
        localStorage.setItem('miss', 0);
    };

    return (
        <div className="loose-container">
            <img className="loose-container__img" src={gameOver} alt="gameover"/>
            <Link className="loose-container__redirect__button__link" to={'/home'}
                  onClick={() => resetState()}>
                <button className="loose-container__redirect__button" type="button">
                    Recommencer
                </button>
            </Link>
        </div>

    );
};

export default Loose;