import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import '../../styles/Home.scss';
import logo from '../../assets/img/logoTrivia.svg';
import {Grid, Row, Col} from 'react-flexbox-grid';

const Home = ({categories}) => (

    <Grid fluid>
        <Row start="xs">
            <Col xs={12} md={8} sm={8} lg={8} className="logoContainer">
                <img className="logo" src={logo} alt="logo"/>
            </Col>

            <Col xs={6} md={2} sm={2} lg={2} className="logoContainer">
                <div className="boxContainer">
                    <h2 className="textNumbers">Score : {(localStorage.getItem('score') || 0)} / 10</h2>
                </div>
            </Col>

            <Col xs={6} md={2} sm={2} lg={2} className="logoContainer">
                <div className="boxContainer">
                    <h2 className="textNumbers">Essais : {(localStorage.getItem('miss') || 0)} / 3</h2>
                </div>
            </Col>
        </Row>

        {categories.length > 0 && (
            <Row>
                {categories.map(category => (
                    <Col xs={12} md={2} sm={3} lg={2} className="fashion">
                        <Link className="text" to={`/categories/${category.id}`} key={category.id}
                              onClick={() => localStorage.setItem('category', `${category.id}`)}>
                            {category.title}
                        </Link>
                    </Col>
                ))}

            </Row>
        )}
    </Grid>
);

Home.propTypes = {
    categories: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number,
            title: PropTypes.string,
            clues_count: PropTypes.number
        }),
    ),
};

export default Home;