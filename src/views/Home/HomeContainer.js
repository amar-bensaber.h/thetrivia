import React, {Component} from 'react';
import Home from './Home';
import api from '../../helpers/api';

class HomeContainer extends Component {
    state = {
        categories: null,
    };

    async componentDidMount() {
        const data = await api.getCategories();
        this.setState({
            categories: data,
        });
    }

    render() {
        const {categories} = this.state;
        if (!categories) return <div className="loader"></div>;

        return (
            <Home categories={this.state.categories}/>
        );
    }
}

export default HomeContainer;