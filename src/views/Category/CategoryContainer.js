import React, {Component, createRef} from 'react';
import api from '../../helpers/api';
import Category from './Category';
import '../../styles/Category.scss';


class CategoryContainer extends Component {

    state = {
        category: null,
        currentQuestion: (JSON.parse(localStorage.getItem(`currentQuestion${localStorage.getItem('category')}`)) || 0),
        score: (localStorage.getItem('score') || 0),
        miss: (localStorage.getItem('miss') || 0),
        inputColor: '',
        solution: false,
        accessSolution: false,
    };


    // createRef in order to bring back input value to its parent
    answerInput = createRef();

    // method to initialize to the localStorage values which are not set yet
    initializeLocalStorage = () => {
        if (!localStorage.getItem('score')) {
            localStorage.setItem('score', 0);
        }

        if (!localStorage.getItem(`currentQuestion${this.state.category.id}`)) {
            localStorage.setItem(`currentQuestion${this.state.category.id}`, 0);
        }

        if (!localStorage.getItem('miss')) {
            localStorage.setItem('miss', 0);
        }
    };

    // async needed when using promise
    async componentDidMount() {
        const data = await api.getCategoryById(this.props.match.params.id);
        // stored response in the state;
        this.setState({
            category: data
        });

        // callback the method to initialize localStorage
        this.initializeLocalStorage();
        this.loose();
        this.win();
    }

    // method which check if the value present in the answer field match with the answer corresponding to the question on the API.
    checkAnswer = () => {
        const answer = this.answerInput.current.value;

        if (answer === this.state.category.clues[this.state.currentQuestion].answer) {
            const addScore = JSON.parse(localStorage.getItem('score')) + 1;
            localStorage.setItem('score', JSON.stringify(addScore));

            const nextQuestion = JSON.parse(localStorage.getItem(`currentQuestion${this.state.category.id}`)) + 1;
            localStorage.setItem(`currentQuestion${this.state.category.id}`, JSON.stringify(nextQuestion));

            this.setState({
                currentQuestion: localStorage.getItem(`currentQuestion${this.state.category.id}`),
                score: localStorage.getItem('score'),
                inputColor: '#6ab132',
                solution: false,
                accessSolution: false
            });

            setTimeout(() => {
                this.setState({inputColor: '#F0F0F0'});
            }, 300);

        } else if (answer !== this.state.category.clues[this.state.currentQuestion].answer) {
            const addMiss = JSON.parse(localStorage.getItem('miss')) + 1;
            localStorage.setItem('miss', JSON.stringify(addMiss));

            this.setState({
                miss: localStorage.getItem('miss'),
                inputColor: '#ff0000',
                accessSolution: true
            });

            setTimeout(() => {
                this.setState({inputColor: '#F0F0F0'});
            }, 300);
        }
    };

    // After submit the form we make the field empty
    emptyField = () => {
        this.answerInput.current.value = '';
    };

    // method which verify if the score is equal to 10 and then show a win response
    win = () => {
        const winScore = JSON.parse(localStorage.getItem('score'));
        if (winScore === 10) {
            this.props.history.push('/win');
        }
    };

    // method which verify if the try number is equal to 3 and then show a loose response
    loose = () => {
        const missAnswer = JSON.parse(localStorage.getItem('miss'));
        if (missAnswer === 3) {
            this.props.history.push('/loose');
        }
    };

    // method which show solution on click

    showSolution = () => {
        this.setState({
            solution: true,
        });
    };

    handleSubmit = (event) => {
        event.preventDefault();
        // callback methods
        this.checkAnswer();
        this.emptyField();
        this.loose();
        this.win();
    };

    // method which reset the score to 0
    removeScore = () => {
        const scoreLocal = this.state.score;

        if (scoreLocal > 0) {
            localStorage.setItem('score', 0);
        }

        this.setState({
            score: localStorage.getItem('score')
        });
    };

    render() {
        const {category, currentQuestion, score, miss, inputColor, solution, accessSolution} = this.state;
        // at first render, category will be null so we need to wait
        // before using data.
        if (!category) return <div className="loader"></div>;

        return (
            <Category
                category={category}
                currentQuestionIndex={currentQuestion}
                score={score}
                miss={miss}
                handleSubmit={this.handleSubmit}
                removeScore={this.removeScore}
                answerInput={this.answerInput} // plug createRef to children
                inputColor={inputColor}
                solution={solution}
                accessSolution={accessSolution}
                showSolution={this.showSolution}
            />
        );
    }
}

export default CategoryContainer;