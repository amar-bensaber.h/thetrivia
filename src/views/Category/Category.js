import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import '../../styles/Category.scss';
import logoTrivia from '../../assets/img/logoTrivia.svg';
import leftArrow from '../../assets/img/left-arrow.svg';

const Category = ({category, currentQuestionIndex, score, miss, inputColor, solution, accessSolution, showSolution, handleSubmit, removeScore, answerInput}) => {

    const currentQuestion = category.clues[currentQuestionIndex];
    return (
        <div className="triviaQuestion-container">
            <div className="triviaQuestion-container-top">
                <div className="triviaQuestion-container-top-header">
                    <img className="triviaQuestion-container-top-header__logo" src={logoTrivia} alt="Logo Trivia"/>
                    <div className="triviaQuestion-container-top-header__panel">
                        <div className="triviaQuestion-container-top-header__panel__score">
                            <h3 className="triviaQuestion-container-top-header__panel__score__title">Score</h3>
                            <h3 className="triviaQuestion-container-top-header__panel__score__result">{score} / 10</h3>
                        </div>
                        <div className="triviaQuestion-container-top-header__panel__miss">
                            <h3 className="triviaQuestion-container-top-header__panel__miss__title">Essais</h3>
                            <h3 className="triviaQuestion-container-top-header__panel__miss__result">{miss} / 3</h3>
                        </div>
                    </div>
                </div>
                <div className="triviaQuestion-container-top__text">
                    <h2 className="triviaQuestion-container-top__category">Catégorie : {category.title}</h2>
                    {currentQuestion !== undefined ?
                        <h3 className="triviaQuestion-container-top__question">{currentQuestion.question}</h3>
                        : <h3 className="triviaQuestion-container-top__question">Tu as fini les questions de cette
                            catégorie ! <Link to={'/home'}
                                              className="triviaQuestion-container-top__question__link"> Retour aux
                                catégories</Link></h3>
                    }
                </div>
            </div>
            <div className="triviaQuestion-container-bottom">
                <div className="triviaQuestion-container-bottom-solution">
                    {accessSolution === true && (
                        <button className="triviaQuestion-container-bottom-solution__button"
                                onClick={showSolution}>Afficher la réponse pour continué</button>
                    )}
                    {solution === true && (
                        <span
                            className="triviaQuestion-container-bottom-solution__text">Solution : {currentQuestion.answer}</span>
                    )}
                </div>
                <form className="triviaQuestion-container-bottom-form" onSubmit={handleSubmit}>
                    <div className="triviaQuestion-container-bottom-question">
                        {/* We give the ref below in order check the value */}
                        <input style={{backgroundColor: inputColor}}
                               className="triviaQuestion-container-bottom-question__answerInput" ref={answerInput}/>
                        <button className="triviaQuestion-container-bottom-question__button" type="submit">VALIDER
                        </button>
                    </div>

                </form>
                <div className="triviaQuestion-container-bottom-options">
                    <Link to={'/home'} className="triviaQuestion-container-bottom-options-link">
                        <div className="triviaQuestion-container-bottom-options-left">
                            <img className="triviaQuestion-container-bottom-options__img" src={leftArrow}
                                 alt="Left Arrow"/>
                            <span className="triviaQuestion-container-bottom-options__return">Retour
                                catégories</span>
                        </div>
                    </Link>
                    <button className="triviaQuestion-container-bottom-options__reset" onClick={removeScore}>Reset le
                        score
                    </button>
                </div>
            </div>
        </div>
    );
};

Category.propTypes = {
    category: PropTypes.shape({}).isRequired,
    handleSubmit: PropTypes.func.isRequired,
    removeScore: PropTypes.func.isRequired,
    answerInput: PropTypes.shape({
        value: PropTypes.instanceOf(HTMLInputElement)
    }),
};

export default Category;