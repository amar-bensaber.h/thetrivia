/* In this helper file we fetch JSON with Async Await */

class api {
    async getCategories() {
        // await response of fetch call
        const response = await fetch('http://jservice.io/api/categories?count=10');
        // only proceed once promise is resolved
        return await response.json();
    }

    async getCategoryById(id) {
        const response = await fetch(`http://jservice.io/api/category?id=${id}`);
        return await response.json();
    }
}

export default new api();
