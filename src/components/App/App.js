import React, {Component, Fragment} from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import './App.scss';
import Welcoming from '../../views/Welcoming/Welcoming';
import HomeContainer from '../../views/Home/HomeContainer';
import CategoryContainer from '../../views/Category/CategoryContainer';
import Win from '../../views/Win/Win';
import Loose from '../../views/Loose/Loose';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Router>
                    <Fragment>
                        <Route exact path="/" component={Welcoming}/>
                        <Route exact path="/home" component={HomeContainer}/>
                        <Route path="/categories/:id" component={CategoryContainer}/>
                        <Route exact path="/win" component={Win}/>
                        <Route exact path="/loose" component={Loose}/>
                    </Fragment>
                </Router>
            </div>
        );
    }
}

export default App;
